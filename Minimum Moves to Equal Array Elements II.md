Given an integer array nums of size n, return the minimum number of moves required to make all array elements equal.

In one move, you can increment or decrement an element of the array by 1.

Test cases are designed so that the answer will fit in a 32-bit integer.

 

Example 1:

Input: nums = [1,2,3]
Output: 2
Explanation:
Only two moves are needed (remember each move increments or decrements one element):
[1,2,3]  =>  [2,2,3]  =>  [2,2,2]
Example 2:

Input: nums = [1,10,2,9]
Output: 16
 

Constraints:

n == nums.length
1 <= nums.length <= 105
-109 <= nums[i] <= 109

Explanation :
After reading this question, your first thought would be a solution with average. Don't worry, when I started with this question, I successfully wasted 10 minutes, trying to solve it with average. Better to give you one example to prove it.

Note : As we are incrementing and decrementing the numbers given in the array by 1, we know the steps with be
| [average] - [current number in the index] |

Example :
Input : [1,0,0,8,6]
average : 3 ( (1 + 0 + 0 + 8 + 6 )/ 5 )
steps required : 2 + 3 + 3 + 5 + 3 = 16

Lets try with Median. But why though?

Input : [1,0,0,8,6]
median : 1 [ 0 , 0 , 1 <-Median , 6 , 8]
steps required : 0 + 1 + 1 + 7 + 5 = 14

By using average, we are trying to bring all numbers in the array to the average, which might not be the minimum steps. But using the median, (which is the average of the middle values of a sorted array) gives us the best point where the steps required are minimum.

Java Solution :

public int minMoves2(int[] nums) {
	Arrays.sort(nums);
	int steps = 0;
	int median = nums[nums.length/2];
	for (int i = 0 ; i < nums.length ; i++) 
	    steps += Math.abs(median - nums[i]);
	return steps;
}