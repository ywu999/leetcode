package com.leetcode.issue322;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SolutionTest {
	Solution solution = new Solution();

	@Test
	void test() {
		int[] coins = new int[] {1,2,5};
		
		assertEquals(solution.coinChange(coins, 11), 3);
		
	}

}
