You are given an array of words where each word consists of lowercase English letters.

wordA is a predecessor of wordB if and only if we can insert exactly one letter anywhere in wordA without changing the order of the other characters to make it equal to wordB.

For example, "abc" is a predecessor of "abac", while "cba" is not a predecessor of "bcad".
A word chain is a sequence of words [word1, word2, ..., wordk] with k >= 1, where word1 is a predecessor of word2, word2 is a predecessor of word3, and so on. A single word is trivially a word chain with k == 1.

Return the length of the longest possible word chain with words chosen from the given list of words.

 

Example 1:

Input: words = ["a","b","ba","bca","bda","bdca"]
Output: 4
Explanation: One of the longest word chains is ["a","ba","bda","bdca"].
Example 2:

Input: words = ["xbc","pcxbcf","xb","cxbc","pcxbc"]
Output: 5
Explanation: All the words can be put in a word chain ["xb", "xbc", "cxbc", "pcxbc", "pcxbcf"].
Example 3:

Input: words = ["abcd","dbqca"]
Output: 1
Explanation: The trivial word chain ["abcd"] is one of the longest word chains.
["abcd","dbqca"] is not a valid word chain because the ordering of the letters is changed.
 

Constraints:

1 <= words.length <= 1000
1 <= words[i].length <= 16
words[i] only consists of lowercase English letters.

# Solution
/**
 * @param {string[]} words
 * @return {number}
 */
var longestStrChain = function(words) {
    var m = Array(words.length).fill(0);
    var max = 0;
    words.sort((a, b) => {
        return a.length - b.length;
    });

    for (var i = 0; i < words.length; i++) {
        for (var j = i + 1; j < words.length; j++) {
            if (isP(words[i], words[j])) {
                m[j] = Math.max(m[i] + 1, m[j]);
                max = Math.max(m[j], max);
            }
        }
    }
    return max + 1;
};

var isP = (w1, w2) => {
    if (w1.length != w2.length - 1) return false;
    var i = 0;
    var j = 0;
    var diff = 0;
    while (i < w1.length && j < w2.length) {
        if (w1.charAt(i) == w2.charAt(j)) {
            i++;
            j++;
        } else {
            diff++;
            j++;
        }
    }
    diff += w1.length - i + w2.length - j;
    return diff == 1;
};