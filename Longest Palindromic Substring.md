Given a string s, return the longest palindromic substring in s.

 

Example 1:

Input: s = "babad"
Output: "bab"
Explanation: "aba" is also a valid answer.
Example 2:

Input: s = "cbbd"
Output: "bb"
 

Constraints:

1 <= s.length <= 1000
s consist of only digits and English letters.

# Solution
class Solution {
    public String longestPalindrome(String s) {
        String longest = "";
        for (int i = 0; i < s.length(); i++) {
            int offset = 0;
            while (true) {
                if (i + offset > s.length() - 1 || i - offset < 0) break;
                if (s.charAt(i + offset) == s.charAt(i - offset)) {
                    if (2 * offset + 1 > longest.length()) {
                        longest = s.substring(i - offset, i + offset + 1);
                    }
                } else {
                    break;
                }
                offset++;
            }
            offset = 0;
            while (true) {
                if (i - offset < 0 || i + offset + 1 > s.length() - 1) break;
                if (s.charAt(i + offset + 1) == s.charAt(i - offset)) {
                    if (2 * offset + 2 > longest.length()) {
                        longest = s.substring(i - offset, i + offset + 2);
                    }
                } else {
                    break;
                }
                offset++;
            }
        }
        return longest;
    }
}