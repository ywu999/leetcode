Given a string s and an array of strings words, return the number of words[i] that is a subsequence of s.

A subsequence of a string is a new string generated from the original string with some characters (can be none) deleted without changing the relative order of the remaining characters.

For example, "ace" is a subsequence of "abcde".
 

Example 1:

Input: s = "abcde", words = ["a","bb","acd","ace"]
Output: 3
Explanation: There are three strings in words that are a subsequence of s: "a", "acd", "ace".
Example 2:

Input: s = "dsahjpjauf", words = ["ahjpjau","ja","ahbwzgqnuk","tnmlanowax"]
Output: 2
 

Constraints:

1 <= s.length <= 5 * 104
1 <= words.length <= 5000
1 <= words[i].length <= 50
s and words[i] consist of only lowercase English letters.

# Solution
class Solution {
    public int numMatchingSubseq(String s, String[] words) {
        List<int[]> arr[] = new LinkedList[128] ;
        for(int i = 0 ; i< 128 ; i++)
            arr[i] = new LinkedList<>() ;
        for(int i = 0 ; i< words.length ; i++)
            arr[words[i].charAt(0)].add(new int[]{i, 0}) ;
        int count = 0 ;
        for(int i = 0 ; i< s.length() ; i++){
            char c = s.charAt(i) ;
            int size = arr[c].size() ;
            for(int j = 0 ; j< size ; j++){
                int[] x = arr[c].get(0) ;
                arr[c].remove(0);
                if(x[1] < words[x[0]].length()-1 ){
                    char y = words[x[0]].charAt(x[1]+1) ;
                    arr[y].add(new int[]{x[0], x[1]+1}) ;
                }
                else //x[1] == words[x[0]].length()-1
                    count++ ;
            }
        }
        return count ;
    }
}