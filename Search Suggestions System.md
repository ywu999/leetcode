You are given an array of strings products and a string searchWord.

Design a system that suggests at most three product names from products after each character of searchWord is typed. Suggested products should have common prefix with searchWord. If there are more than three products with a common prefix return the three lexicographically minimums products.

Return a list of lists of the suggested products after each character of searchWord is typed.

 

Example 1:

Input: products = ["mobile","mouse","moneypot","monitor","mousepad"], searchWord = "mouse"
Output: [
["mobile","moneypot","monitor"],
["mobile","moneypot","monitor"],
["mouse","mousepad"],
["mouse","mousepad"],
["mouse","mousepad"]
]
Explanation: products sorted lexicographically = ["mobile","moneypot","monitor","mouse","mousepad"]
After typing m and mo all products match and we show user ["mobile","moneypot","monitor"]
After typing mou, mous and mouse the system suggests ["mouse","mousepad"]
Example 2:

Input: products = ["havana"], searchWord = "havana"
Output: [["havana"],["havana"],["havana"],["havana"],["havana"],["havana"]]
Example 3:

Input: products = ["bags","baggage","banner","box","cloths"], searchWord = "bags"
Output: [["baggage","bags","banner"],["baggage","bags","banner"],["baggage","bags"],["bags"]]
 

Constraints:

1 <= products.length <= 1000
1 <= products[i].length <= 3000
1 <= sum(products[i].length) <= 2 * 104
All the strings of products are unique.
products[i] consists of lowercase English letters.
1 <= searchWord.length <= 1000
searchWord consists of lowercase English letters.

# Solution
class Solution {
    public List < List < String >> suggestedProducts(String[] products, String searchWord) {
        if (products == null || products.length == 0)
            return null;
        if (searchWord == null || searchWord.length() == 0)
            return null;
        Trie trie = new Trie();

        //Because we want lexicographically sorted result when there are multiple matches
        Arrays.sort(products);

        //Insert into trie
        for (int i = 0; i < products.length; i++) {
            trie.insert(products[i], i);
        }
        //search the word
        List < List < Integer >> indexList = trie.getIndexes(searchWord);
        List < List < String >> result = new ArrayList < > ();
        for (List < Integer > tempList: indexList) {
            //we want maximum 3 matches in result 
            int limit = Math.min(3, tempList.size());
            List < String > tempResult = new ArrayList < > (limit);
            for (int i = 0; i < limit; i++) {
                tempResult.add(products[tempList.get(i)]);
            }
            result.add(tempResult);
        }
        return result;
    }
}

class TrieNode {
    Character ch;
    Map < Character, TrieNode > children;
    List < Integer > indexes;
    public TrieNode(Character ch) {
        this.ch = ch;
        children = new HashMap < > ();
        indexes = new ArrayList < > ();
    }
}

class Trie {
    Map < Character, TrieNode > wordTree;

    public Trie() {
        wordTree = new HashMap < > ();
    }

    public void insert(String word, int index) {
        if (word == null || word.length() == 0)
            return;
        Character firstChar = word.charAt(0);
        TrieNode node = wordTree.get(firstChar);
        if (node == null) {
            node = new TrieNode(firstChar);
            wordTree.put(firstChar, node);
        }
        node.indexes.add(index);

        for (int i = 1; i < word.length(); i++) {
            Character ch = word.charAt(i);
            if (!node.children.containsKey(ch)) {
                node.children.put(ch, new TrieNode(ch));
            }
            node.children.get(ch).indexes.add(index);
            node = node.children.get(ch);
        }
    }

    public List < List < Integer >> getIndexes(String word) {
        List < List < Integer >> result = new ArrayList < > ();
        for (int i = 1; i <= word.length(); i++) {
            result.add(getIndexesForOneWholeWord(word.substring(0, i)));
        }
        return result;
    }

    private List < Integer > getIndexesForOneWholeWord(String word) {
        if (word == null || word.length() == 0)
            return new ArrayList < > ();

        TrieNode node = wordTree.get(word.charAt(0));
        if (node == null)
            return new ArrayList < > ();
        for (int i = 1; i < word.length(); i++) {
            Character ch = word.charAt(i);
            node = node.children.get(ch);
            if (node == null) {
                break;
            }
        }
        if (node == null)
            return new ArrayList < > ();
        return node.indexes;
    }
}

# Solution 2
[["tkruiswrcbzsbkwbhhvjzzuuiayqzbxjosjssacislcvbtcojpmyatkfgyx","txryxhtutwdrqmpcapbcrlmhzsobueefwfekusmmylr","tyqcpfudqjrabwwvdvwmsyscnazaxpsjjhetouegipqevvointclztzummwrrbntjlsj"],["tyqcpfudqjrabwwvdvwmsyscnazaxpsjjhetouegipqevvointclztzummwrrbntjlsj","tyqcpfvacyrjvmadrmntxotgvgivdvcuwygpjfwcuppunolukrum","tyqcpfvjijiwoup"],["tyqcpfudqjrabwwvdvwmsyscnazaxpsjjhetouegipqevvointclztzummwrrbntjlsj","tyqcpfvacyrjvmadrmntxotgvgivdvcuwygpjfwcuppunolukrum","tyqcpfvjijiwoup"],["tyqcpfudqjrabwwvdvwmsyscnazaxpsjjhetouegipqevvointclztzummwrrbntjlsj","tyqcpfvacyrjvmadrmntxotgvgivdvcuwygpjfwcuppunolukrum","tyqcpfvjijiwoup"],["tyqcpfudqjrabwwvdvwmsyscnazaxpsjjhetouegipqevvointclztzummwrrbntjlsj","tyqcpfvacyrjvmadrmntxotgvgivdvcuwygpjfwcuppunolukrum","tyqcpfvjijiwoup"],["tyqcpfudqjrabwwvdvwmsyscnazaxpsjjhetouegipqevvointclztzummwrrbntjlsj","tyqcpfvacyrjvmadrmntxotgvgivdvcuwygpjfwcuppunolukrum","tyqcpfvjijiwoup"],["tyqcpfvacyrjvmadrmntxotgvgivdvcuwygpjfwcuppunolukrum","tyqcpfvjijiwoup","tyqcpfvobzfvb...

    public List<List<String>> suggestedProducts(String[] products, String searchWord) {
        Arrays.sort(products);
        List<List<String>> output = new ArrayList<>();
        List<String> matches = new ArrayList<String>();
        for(String product:products) {matches.add(product);}

        for(int i=0; i<searchWord.length(); i++) {
            char c = searchWord.charAt(i);
            List<String> newMatches = new ArrayList<>();
            List<String> foundMatches = new ArrayList<>();
            for(int j=0; j<matches.size(); j++) {
                String candidate = matches.get(j);
                if(i>=candidate.length()) continue;
                char d = candidate.charAt(i);
                if(d == c) {
                    newMatches.add(candidate);
                    if(foundMatches.size()<3) foundMatches.add(candidate);
                } else if (d>c) {
                    break;
                }
            }
            matches = newMatches;
            output.add(foundMatches);
        }
        return output;
    }