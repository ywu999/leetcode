You are given a rectangular cake of size h x w and two arrays of integers horizontalCuts and verticalCuts where:

horizontalCuts[i] is the distance from the top of the rectangular cake to the ith horizontal cut and similarly, and
verticalCuts[j] is the distance from the left of the rectangular cake to the jth vertical cut.
Return the maximum area of a piece of cake after you cut at each horizontal and vertical position provided in the arrays horizontalCuts and verticalCuts. Since the answer can be a large number, return this modulo 109 + 7.

 

Example 1:


Input: h = 5, w = 4, horizontalCuts = [1,2,4], verticalCuts = [1,3]
Output: 4 
Explanation: The figure above represents the given rectangular cake. Red lines are the horizontal and vertical cuts. After you cut the cake, the green piece of cake has the maximum area.
Example 2:


Input: h = 5, w = 4, horizontalCuts = [3,1], verticalCuts = [1]
Output: 6
Explanation: The figure above represents the given rectangular cake. Red lines are the horizontal and vertical cuts. After you cut the cake, the green and yellow pieces of cake have the maximum area.
Example 3:

Input: h = 5, w = 4, horizontalCuts = [3], verticalCuts = [3]
Output: 9
 

Constraints:

2 <= h, w <= 109
1 <= horizontalCuts.length <= min(h - 1, 105)
1 <= verticalCuts.length <= min(w - 1, 105)
1 <= horizontalCuts[i] < h
1 <= verticalCuts[i] < w
All the elements in horizontalCuts are distinct.
All the elements in verticalCuts are distinct.

# Solution
class Solution {
    public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);

        int[] hc = new int[horizontalCuts.length + 2];
        for (int i = 1; i < hc.length - 1; i++) {
            hc[i] = horizontalCuts[i - 1];
        }
        hc[hc.length - 1] = h;

        int[] vc = new int[verticalCuts.length + 2];
        for (int j = 1; j < vc.length - 1; j++) {
            vc[j] = verticalCuts[j - 1];
        }
        vc[vc.length - 1] = w;

        int maxH = 0;
        for (int i = 1; i < hc.length; i++) {
            int max = (hc[i] - hc[i - 1]);
            if (maxH < max) maxH = max;
        }
        int maxV = 0;
        for (int j = 1; j < vc.length; j++) {
            int max = (vc[j] - vc[j - 1]);
            if (maxV < max) maxV = max;
        }
        return maxH * maxV;
    }
}