You are given an integer array nums and you have to return a new counts array. The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].

 

Example 1:

Input: nums = [5,2,6,1]
Output: [2,1,1,0]
Explanation:
To the right of 5 there are 2 smaller elements (2 and 1).
To the right of 2 there is only 1 smaller element (1).
To the right of 6 there is 1 smaller element (1).
To the right of 1 there is 0 smaller element.
Example 2:

Input: nums = [-1]
Output: [0]
Example 3:

Input: nums = [-1,-1]
Output: [0,0]
 

Constraints:

1 <= nums.length <= 105
-104 <= nums[i] <= 104

# Solution
class Solution {
    private int getPosition(List < Integer > arr, int val, int s, int e) {
        int m = s + (e - s) / 2;
        if (m == arr.size()) return e;
        if (arr.get(m) == val) {
            if (m == 0) return 0;
            else if (arr.get(m - 1) < val) return m;
            return getPosition(arr, val, s, m - 1);
        } else if (val > arr.get(m)) {
            if (m + 1 == arr.size() || val < arr.get(m + 1)) return m + 1;
            return getPosition(arr, val, m + 1, e);
        } else {
            if (m == 0) return 0;
            else if (val > arr.get(m - 1)) return m;
            return getPosition(arr, val, s, m - 1);
        }
    }
    public List < Integer > countSmaller(int[] nums) {
        int n = nums.length;
        List < Integer > sortedArr = new ArrayList < Integer > ();
        sortedArr.add(nums[n - 1]);
        List < Integer > ret = new ArrayList < Integer > ();
        ret.add(0);
        for (int i = n - 2; i >= 0; --i) {
            int val = getPosition(sortedArr, nums[i], 0, sortedArr.size());
            ret.add(val);
            sortedArr.add(val, nums[i]);
        }
        Collections.reverse(ret);
        return ret;
    }
}