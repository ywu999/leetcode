Given a string s, find the length of the longest substring without repeating characters.

 

Example 1:

Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
 

Constraints:

0 <= s.length <= 5 * 104
s consists of English letters, digits, symbols and spaces.

# Solution
/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {
    var dp = [];
    var max = 0;
    for (var i = 0; i < s.length; i++) {
        dp[i] = 0;
        const set = new Set();
        var setSize = 0;
        for (var j = i; j < s.length; j++) {
            set.add(s.charAt(j));
            if (setSize == set.size) {
                break;
            } else {
                setSize = set.size;
                dp[i] = dp[i] + 1;
            }
        }
        max = Math.max(dp[i], max);
    }
    return max;
};

# Solution
class Solution {
    public int lengthOfLongestSubstring(String s) {
        Map < Character, Integer > map = new HashMap < > ();
        int max = 0;
        int repeat = -1;
        for (int i = 0; i < s.length(); i++) {
            if (map.get(s.charAt(i)) != null) {
                repeat = Math.max(s.charAt(i), repeat);
            }
            max = Math.max(max, i - repeat);
            map.put(s.charAt(i), i);
        }
        return max;
    }
}