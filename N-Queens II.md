The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each other.

Given an integer n, return the number of distinct solutions to the n-queens puzzle.

 

Example 1:


Input: n = 4
Output: 2
Explanation: There are two distinct solutions to the 4-queens puzzle as shown.
Example 2:

Input: n = 1
Output: 1
 

Constraints:

1 <= n <= 9

# Solution
/**
 * @param {number} n
 * @return {number}
 */
var totalNQueens = function(n) {
    var m = [];
    for (var i = 0; i < n; i++) {
        if (m[i] == null) m[i] = [];
        for (var j = 0; j < n; j++)
            m[i][j] = 0;
    }
    count = 0;
    place(0, n, m);
    return count;
};
var count = 0;
var place = (row, n, m, ) => {
    if (row == n) {
        count++;
        return;
    }

    for (var col = 0; col < n; col++) {
        if (isValid(row, col, n, m)) {
            m[row][col] = 1;
            place(row + 1, n, m);
            m[row][col] = 0;
        }
    }
}

var isValid = (row, col, n, m) => {
    //for(var j=0; j<col; j++) {
    //    if(m[row][j] == 1) return false;
    //}
    for (var k = 1; row - k >= 0 && col - k >= 0; k++) {
        if (m[row - k][col - k] == 1) return false;
    }
    for (var k = 1; row - k >= 0 && col + k < n; k++) {
        if (m[row - k][col + k] == 1) return false;
    }
    for (var i = 0; i < row; i++) {
        if (m[i][col] == 1) return false;
    }
    return true;

}