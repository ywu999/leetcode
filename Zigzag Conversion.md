The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);
 

Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"
Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
P     I    N
A   L S  I G
Y A   H R
P     I
Example 3:

Input: s = "A", numRows = 1
Output: "A"
 

Constraints:

1 <= s.length <= 1000
s consists of English letters (lower-case and upper-case), ',' and '.'.
1 <= numRows <= 1000

# Solution
class Solution {
    public String convert(String s, int numRows) {
        char[][] m = new char[numRows][s.length()];
        for (int i = 0; i < numRows; i++)
            Arrays.fill(m[i], ' ');

        int i = 0;
        int j = 0;
        int k = 0;
        boolean fillColumn = true;
        while (true) {
            if (k < s.length()) {
                char c = s.charAt(k);
                k++;
                //System.out.print("Fill "+c);
                if (fillColumn) {
                    m[i][j] = c;
                    //System.out.println(" > "+i+","+j);
                    i++;
                    if (i == numRows) {
                        fillColumn = false;
                        if (numRows >= 2) {
                            i = numRows - 2;
                            j++;
                        } else if (numRows == 1) {
                            i = 0;
                            fillColumn = true;
                            j++;
                        }
                    }
                } else {
                    m[i][j] = c;
                    //System.out.println(" > "+i+","+j);
                    i--;
                    j++;
                    if (i < 0) {
                        fillColumn = true;
                        if (numRows >= 1) {
                            i = 1;
                            j--;
                        }
                    }
                }
            } else {
                break;
            }
        }

        StringBuffer sb = new StringBuffer();
        for (i = 0; i < m.length; i++)
            for (j = 0; j < m[i].length; j++) {
                if (m[i][j] != ' ') sb.append(m[i][j]);
            }
        return sb.toString();
    }
}