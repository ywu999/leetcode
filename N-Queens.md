The n-queens puzzle is the problem of placing n queens on an n x n chessboard such that no two queens attack each other.

Given an integer n, return all distinct solutions to the n-queens puzzle. You may return the answer in any order.

Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a queen and an empty space, respectively.

 

Example 1:


Input: n = 4
Output: [[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
Explanation: There exist two distinct solutions to the 4-queens puzzle as shown above
Example 2:

Input: n = 1
Output: [["Q"]]
 

Constraints:

1 <= n <= 9

# Solution
/**
 * @param {number} n
 * @return {string[][]}
 */
var solveNQueens = function(n) {
    var matrix = [];
    for (var i = 0; i < n; i++) {
        if (matrix[i] == null) matrix[i] = [];
        for (var j = 0; j < n; j++)
            matrix[i][j] = '.';
    }
    var answer = [];
    solve(0, n, matrix, answer);
    return answer;

};

var solve = function(row, n, matrix, answer) {
    if (row == n) {
        let str = [];
        for (var i = 0; i < n; i++) {
            if (str[i] == null) str[i] = "";
            for (var j = 0; j < n; j++)
                str[i] += matrix[i][j];
        }
        answer.push(str);
    }
    for (var col = 0; col < n; col++) {
        if (isValid(row, col, n, matrix)) {
            matrix[row][col] = 'Q';
            solve(row + 1, n, matrix, answer);
            matrix[row][col] = '.';
        }
    }
}

var isValid = function(row, col, n, matrix) {
    //checking row wise
    for (var i = 0; i < row; i++) {
        if (matrix[i][col] == 'Q')
            return false;
    }
    var i = row,
        j = col;
    while (i >= 0 && j >= 0) {
        if (matrix[i][j] == 'Q')
            return false;
        i--;
        j--;
    }
    i = row;
    j = col;
    while (i >= 0 && j < n) {
        if (matrix[i][j] == 'Q')
            return false;
        i--;
        j++;
    }
    return true;
}