Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]
 

Constraints:

2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.

# Solution 1
class Solution {
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++)
            for (int j = 0; j != i && j < nums.length; j++) {
                if (target == nums[i] + nums[j]) {
                    return new int[] {
                        i,
                        j
                    };
                }
            }
        return null;
    }
}

Time compexity O(n^2)
Space compexity O(1)

# Solution 2
class Solution {
    public int[] twoSum(int[] nums, int target) {
        Map < Integer, Integer > map = new HashMap < > ();
        //Key: searched value, value: index
        for (int i = 0; i < nums.length; i++) {
            if (map.get(nums[i]) != null) {
                return new int[] {
                    i,
                    map.get(nums[i])
                };
            }
            map.put(target - nums[i], i);
        }
        return null;
    }
}

Time complexity O(n)
Space complexity O(n)