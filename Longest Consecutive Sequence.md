Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.

You must write an algorithm that runs in O(n) time.

 

Example 1:

Input: nums = [100,4,200,1,3,2]
Output: 4
Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
Example 2:

Input: nums = [0,3,7,2,5,8,4,6,0,1]
Output: 9
 

Constraints:

0 <= nums.length <= 105
-109 <= nums[i] <= 109

# Solution
class Solution {
    public int longestConsecutive(int[] nums) {
        PriorityQueue < Integer > pq = new PriorityQueue < > ();
        for (int i = 0; i < nums.length; i++) {
            pq.add(nums[i]);
        }

        int longest = 0;
        int seq = 0;
        Integer previous = null;
        while (!pq.isEmpty()) {
            int current = pq.poll();
            if (previous == null) {
                seq = 1;
            } else {
                if (current == previous + 1) {
                    seq++;
                } else if (current == previous) {} else {
                    seq = 1;
                }
            }
            previous = current;
            longest = Math.max(seq, longest);
        }
        return longest;
    }
}