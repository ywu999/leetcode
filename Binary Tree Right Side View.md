Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.

 

Example 1:


Input: root = [1,2,3,null,5,null,4]
Output: [1,3,4]
Example 2:

Input: root = [1,null,3]
Output: [1,3]
Example 3:

Input: root = []
Output: []
 

Constraints:

The number of nodes in the tree is in the range [0, 100].
-100 <= Node.val <= 100

# Solution
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    Map < Integer, Integer > map = new HashMap();
    public List < Integer > rightSideView(TreeNode root) {
        bfs(root, 0);
        List < Integer > ret = new ArrayList < Integer > ();
        int i = 0;
        while (true) {
            Integer v = map.get(i);
            if (v == null) break;
            else ret.add(v);
            i++;
        }
        return ret;
    }

    void bfs(TreeNode root, int level) {
        if (root != null) {
            map.put(level, root.val);
            if (root.left != null) bfs(root.left, level + 1);
            if (root.right != null) bfs(root.right, level + 1);
        }
    }
}