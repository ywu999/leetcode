Implement pow(x, n), which calculates x raised to the power n (i.e., xn).

Example 1:

Input: x = 2.00000, n = 10
Output: 1024.00000
Example 2:

Input: x = 2.10000, n = 3
Output: 9.26100
Example 3:

Input: x = 2.00000, n = -2
Output: 0.25000
Explanation: 2-2 = 1/22 = 1/4 = 0.25
 

Constraints:

-100.0 < x < 100.0
-231 <= n <= 231-1
-104 <= xn <= 104

# Solution 0
class Solution {
    public double myPow(double x, int n) {
        if (n == Integer.MIN_VALUE) return myPow(x, n + 1) / x;

        if (n < 0) {
            x = 1.0 / x;
            n = -n;
        }
        double r = 1;
        while (n > 0) {
            if (n % 2 == 1) r = r * x;
            x = x * x;
            n = n / 2;
        }
        return r;
    }
}

Time Complexity O(log(N))
Space Complexity O(1)
but there are problem with the edge case of n of -2(31)-1.

# Solution 1
class Solution {
    public double myPow(double x, int n) {
        if (n == 0) return 1;
        if (n == -1) return 1 / x;
        if (n % 2 == 0) {
            double r = myPow(x, n / 2);
            return r * r;
        } else {
            double r = myPow(x, (n - 1) / 2);
            return r * r * x;
        }
    }
}

Time Complexity O(log(N))
Space Complexity O(log(N))

# Solution 2
import java.lang.Math;
class Solution {
    public double myPow(double x, int n) {
        return Math.pow(x, n);
    }
}

According to https://developer.classpath.org/doc/java/lang/Math-source.html
 557:   public static double pow(double a, double b)
 558:   {
 559:     return VMMath.pow(a,b);
 560:   }
 This is not as efficient as the solution 1.

# Solution 3
class Solution {
    public double myPow(double x, int n) {
        if (n < 0) return 1 / myPow(x, -n);
        double r = 1;
        for (int i = 0; i < n; i++) {
            r = r * x;
        }
        return r;
    }
}

Time Complexity O(N)
Space Complexity O(1)