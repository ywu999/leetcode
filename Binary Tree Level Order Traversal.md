Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).

 

Example 1:


Input: root = [3,9,20,null,null,15,7]
Output: [[3],[9,20],[15,7]]
Example 2:

Input: root = [1]
Output: [[1]]
Example 3:

Input: root = []
Output: []
 

Constraints:

The number of nodes in the tree is in the range [0, 2000].
-1000 <= Node.val <= 1000

# Solution
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    Map<Integer, List<Integer>> map = new HashMap<> ();
    public List<List<Integer>> levelOrder(TreeNode root) {
        travel(map, root, 0);
        List<List<Integer>> ret = new ArrayList<> ();
        for (int level = 0;; level++) {
            List<Integer> list = map.get(level);
            if (list == null) break;
            else ret.add(list);
        }
        return ret;
    }

    void travel(Map < Integer, List < Integer >> map, TreeNode root, int level) {
        if (root != null) {
            List < Integer > list = map.get(level);
            if (list == null) {
                list = new ArrayList < Integer > ();
                map.put(level, list);
            }
            list.add(root.val);
            travel(map, root.left, level + 1);
            travel(map, root.right, level + 1);
        }
    }
}