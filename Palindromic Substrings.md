Given a string s, return the number of palindromic substrings in it.

A string is a palindrome when it reads the same backward as forward.

A substring is a contiguous sequence of characters within the string.

 

Example 1:

Input: s = "abc"
Output: 3
Explanation: Three palindromic strings: "a", "b", "c".
Example 2:

Input: s = "aaa"
Output: 6
Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".
 

Constraints:

1 <= s.length <= 1000
s consists of lowercase English letters.

# Solution
import java.lang.String;
class Solution {
  public int countSubstrings(String s) {
    int count = 0;
    for (int i = 0; i < s.length(); i++) {
      count += count(s, i, i);
      if (i < s.length() - 1) count += count(s, i, i + 1);
    }
    return count;
  }

  int count(String s, int left, int right) {
    int count = 0;
    char[] cs = s.toCharArray();
    while (left >= 0 && right < s.length() && (cs[left] == cs[right])) {
      count++;
      left = left - 1;
      right = right + 1;
    }
    return count;
  }
}
