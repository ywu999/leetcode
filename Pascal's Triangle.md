Given an integer numRows, return the first numRows of Pascal's triangle.

In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:


 

Example 1:

Input: numRows = 5
Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
Example 2:

Input: numRows = 1
Output: [[1]]
 

Constraints:

1 <= numRows <= 30

# Solution
class Solution {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ret = new ArrayList<> ();
        generate(ret, numRows);
        return ret;
    }

    void generate(List<List<Integer>> ret, int depth) {
        if (depth == 1) {
            List<Integer> list = new ArrayList<> ();
            list.add(1);
            ret.add(list);
        } else {
            generate(ret, depth - 1);
            List<Integer> last = ret.get(ret.size() - 1);
            List<Integer> list = new ArrayList < > ();
            for (int i = 0; i < last.size(); i++) {
                if (i == 0) list.add(last.get(0));
                else list.add(last.get(i) + last.get(i - 1));
            }
            list.add(1);
            ret.add(list);
        }
    }
}