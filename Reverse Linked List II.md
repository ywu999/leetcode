Given the head of a singly linked list and two integers left and right where left <= right, reverse the nodes of the list from position left to position right, and return the reversed list.

 

Example 1:


Input: head = [1,2,3,4,5], left = 2, right = 4
Output: [1,4,3,2,5]
Example 2:

Input: head = [5], left = 1, right = 1
Output: [5]
 

Constraints:

The number of nodes in the list is n.
1 <= n <= 500
-500 <= Node.val <= 500
1 <= left <= right <= n
 

Follow up: Could you do it in one pass?

# Solution
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode rev(ListNode temp, int right) {
        ListNode prev = null;
        ListNode start = temp;
        ListNode curr = temp;
        while (right >= 0) {
            ListNode temp1 = curr.next;
            curr.next = prev;
            prev = curr;
            curr = temp1;
            right -= 1;
        }
        start.next = curr;
        return prev;
    }
    public ListNode reverseBetween(ListNode head, int left, int right) {
        if (head == null || left == right || head.next == null) {
            return head;
        }
        ListNode temp = head;
        int temp_l = left;
        ListNode prev = head;
        if (left == 1) {
            return rev(head, right - left);
        }
        while (temp_l > 1) {
            prev = temp;
            temp = temp.next;
            temp_l -= 1;
        }
        prev.next = rev(temp, right - left);
        return head;

    }
}