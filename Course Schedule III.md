There are n different online courses numbered from 1 to n. You are given an array courses where courses[i] = [durationi, lastDayi] indicate that the ith course should be taken continuously for durationi days and must be finished before or on lastDayi.

You will start on the 1st day and you cannot take two or more courses simultaneously.

Return the maximum number of courses that you can take.

 

Example 1:

Input: courses = [[100,200],[200,1300],[1000,1250],[2000,3200]]
Output: 3
Explanation: 
There are totally 4 courses, but you can take 3 courses at most:
First, take the 1st course, it costs 100 days so you will finish it on the 100th day, and ready to take the next course on the 101st day.
Second, take the 3rd course, it costs 1000 days so you will finish it on the 1100th day, and ready to take the next course on the 1101st day. 
Third, take the 2nd course, it costs 200 days so you will finish it on the 1300th day. 
The 4th course cannot be taken now, since you will finish it on the 3300th day, which exceeds the closed date.
Example 2:

Input: courses = [[1,2]]
Output: 1
Example 3:

Input: courses = [[3,2],[4,3]]
Output: 0
 

Constraints:

1 <= courses.length <= 104
1 <= durationi, lastDayi <= 104

# The Optimal Solution
public int scheduleCourse(int[][] courses) {
    Arrays.sort(courses, (a, b) -> a[1] - b[1]);
    PriorityQueue < Integer > takenCourses = new PriorityQueue < > (Collections.reverseOrder());
    int currentTotalTime = 0;

    for (int i = 0; i < courses.length; i++) {
        if (currentTotalTime + courses[i][0] <= courses[i][1]) {
            takenCourses.offer(courses[i][0]);
            currentTotalTime += courses[i][0];
        } else if (!takenCourses.isEmpty() && takenCourses.peek() > courses[i][0]) {
            currentTotalTime += courses[i][0] - takenCourses.poll();
            takenCourses.offer(courses[i][0]);
        }
    }
    return takenCourses.size();
}

# The Iteration Solution
    public int scheduleCourse(int[][] courses) {
        Arrays.sort(courses, (a, b) -> a[1] - b[1]);

        int currentTotalTime = 0;
        int count = 0;

        for (int i = 0; i < courses.length; i++) {
            if (currentTotalTime + courses[i][0] <= courses[i][1]) {
                currentTotalTime += courses[i][0];
                count++;
            } else {
                int max_i = i;
                for (int j = 0; j < i; j++) {
                    if (courses[j][0] > courses[max_i][0])
                        max_i = j;
                }
                currentTotalTime += courses[i][0] - courses[max_i][0];
                courses[max_i][0] = -1;
            }
        }
        return count;
    }

# The Incorrect Implementation
    public int scheduleCourse(int[][] courses) {
        Arrays.sort(courses, (int[] e, int[] e2) -> {
            return e[1] - e[0] + e2[0] - e2[1];
        });
        int count = 0;
        int start = 0;
        int i = 0;
        int taken = take(courses, i, start);
        return taken;
    }

    int take(int[][] courses, int i, int start) {
        if (i > courses.length - 1) return 0;
        else {
            if (courses[i][0] > courses[i][1] - start) {
                return take(courses, i + 1, start);
            } else {
                int take1 = take(courses, i + 1, start);
                int take2 = take(courses, i + 1, start + courses[i][0]) + 1;
                return take1 > take2 ? take1 : take2;
            }
        }
    }
