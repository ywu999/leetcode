Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

If target is not found in the array, return [-1, -1].

You must write an algorithm with O(log n) runtime complexity.

 

Example 1:

Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
Example 2:

Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
Example 3:

Input: nums = [], target = 0
Output: [-1,-1]
 

Constraints:

0 <= nums.length <= 105
-109 <= nums[i] <= 109
nums is a non-decreasing array.
-109 <= target <= 109

# Solution
class Solution {
    public int[] searchRange(int[] nums, int target) {
        if (nums.length == 0) return new int[] {
            -1, -1
        };
        int start = 0;
        int end = nums.length - 1;
        boolean found = false;
        int previous = -1;
        int middle = -1;

        while (true) {
            middle = (start + end) / 2;
            if (target < nums[middle]) end = middle - 1;
            if (target > nums[middle]) start = middle + 1;

            if (previous == middle) break;
            previous = middle;

        }

        if (target == nums[middle]) {
            start = middle;
            end = middle;
            for (int i = 0;; i++) {
                if (middle - i >= 0 && nums[middle - i] == target) {
                    start = middle - i;
                } else break;
            }
            for (int i = 0;; i++) {
                if (middle + i < nums.length && nums[middle + i] == target) {
                    end = middle + i;
                } else break;
            }
        } else {
            start = -1;
            end = -1;
        }

        return new int[] {
            start,
            end
        };
    }
}