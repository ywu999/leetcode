Write an efficient algorithm that searches for a value target in an m x n integer matrix matrix. This matrix has the following properties:

Integers in each row are sorted in ascending from left to right.
Integers in each column are sorted in ascending from top to bottom.
 

Example 1:


Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 5
Output: true
Example 2:


Input: matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 20
Output: false
 

Constraints:

m == matrix.length
n == matrix[i].length
1 <= n, m <= 300
-109 <= matrix[i][j] <= 109
All the integers in each row are sorted in ascending order.
All the integers in each column are sorted in ascending order.
-109 <= target <= 109

# Solution
class Solution {
    int[][] matrix;
    int m;
    int n;
    int target;
    boolean[][] visited;
        
    public boolean searchMatrix(int[][] matrix, int target) {
        this.matrix = matrix;
        this.target = target;
        m = matrix.length;
        n = matrix[0].length;
        visited = new boolean[m][n];
        return search(0, 0);
    }
    
    boolean search(int i, int j) {
        boolean ret = false;     
        if(i<0||j<0||i>m-1 || j>n-1) return false;
        else if(visited[i][j]) ret = false;
        else { visited[i][j] = true;
            if(matrix[i][j] == target) ret=true;
             else if(matrix[i][j]<target) {
                ret =  search(i+1, j)||search(i+1, j+1)||search(i, j+1);
            } else {
                ret = search(i-1, j) || search(i-1, j-1) || search(i, j-1);
            }
        } 
        return ret;
    }
}

class Solution {
        
    public boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length;
        int n = matrix[0].length;
        for(int i=0; i<m; i++)
            for(int j=0; j<n; j++) 
                if(matrix[i][j] == target) return true;
        return false;
    }
    
}
 