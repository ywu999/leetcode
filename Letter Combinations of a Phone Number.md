Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. Return the answer in any order.

A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.

Example 1:

Input: digits = "23"
Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]
Example 2:

Input: digits = ""
Output: []
Example 3:

Input: digits = "2"
Output: ["a","b","c"]
 

Constraints:

0 <= digits.length <= 4
digits[i] is a digit in the range ['2', '9'].

# Solution
import java.util.Map;
class Solution {
    Map<Character, String> map = new HashMap<>();
    public List<String> letterCombinations(String digits) {
        if(map.isEmpty()) {
            map.put('2', "abc");
            map.put('3', "def");
            map.put('4', "ghi");
            map.put('5', "jkl");
            map.put('6', "mno");
            map.put('7', "pqrs");
            map.put('8', "tuv");
            map.put('9', "wxyz");
        }
        
        List<String> output = new ArrayList<>();
        find(digits, output);
        return output;
    }
    private void find(String digits,List<String> output) {
        if(digits.length() == 0) {
            
        } else if(digits.length() == 1) {
            char c = digits.charAt(0);
            String s = map.get(c);
            for(char c1:s.toCharArray()) {
                output.add(""+c1);
            }
        } else {
            String subdigits = digits.substring(0, digits.length()-1);
            find(subdigits, output);
            String s = map.get(digits.charAt(digits.length()-1));
            char[] cs = s.toCharArray();
            List<String> newOutput = new ArrayList<String>();
            for(String os:output) {
                for(char c:cs) {
                    newOutput.add(os+c);
                }
            }
            output.clear();
            output.addAll(newOutput);
        }
    }
}