Given a matrix and a target, return the number of non-empty submatrices that sum to target.

A submatrix x1, y1, x2, y2 is the set of all cells matrix[x][y] with x1 <= x <= x2 and y1 <= y <= y2.

Two submatrices (x1, y1, x2, y2) and (x1', y1', x2', y2') are different if they have some coordinate that is different: for example, if x1 != x1'.

 

Example 1:


Input: matrix = [[0,1,0],[1,1,1],[0,1,0]], target = 0
Output: 4
Explanation: The four 1x1 submatrices that only contain 0.
Example 2:

Input: matrix = [[1,-1],[-1,1]], target = 0
Output: 5
Explanation: The two 1x2 submatrices, plus the two 2x1 submatrices, plus the 2x2 submatrix.
Example 3:

Input: matrix = [[904]], target = 0
Output: 0
 

Constraints:

1 <= matrix.length <= 100
1 <= matrix[0].length <= 100
-1000 <= matrix[i] <= 1000
-10^8 <= target <= 10^8

# Solution
/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {number}
 */
var numSubmatrixSumTarget = function(matrix, target) {
    var sumMatrix = [],
        sum, hash = {},
        counterPart, ans = 0,
        prefixSum = 0;
    //Do rowwise sum
    for (let i = 0; i < matrix.length; i++) {
        sum = 0;
        for (let j = 0; j < matrix[i].length; j++) {
            sum += matrix[i][j];
            if (sumMatrix[i] === undefined) {
                sumMatrix[i] = [];
            }
            sumMatrix[i][j] = sum;
        }
    }

    //Do columnwise sum
    for (let j = 0; j < matrix[0].length; j++) {
        sum = 0;
        for (let i = 0; i < matrix.length; i++) {
            sum += sumMatrix[i][j];
            if (sumMatrix[i] === undefined) {
                sumMatrix[i] = [];
            }
            sumMatrix[i][j] = sum;
        }
    }
    //Check all submatrices one by one 
    for (let i1 = 0; i1 < sumMatrix.length; i1++) {
        for (let i2 = i1; i2 < sumMatrix.length; i2++) {
            hash = {};
            hash[0] = 1;
            for (let j = 0; j < sumMatrix[i1].length; j++) {
                if (i1 > 0) {
                    prefixSum = sumMatrix[i2][j] - sumMatrix[i1 - 1][j];
                } else {
                    prefixSum = sumMatrix[i2][j];
                }
                counterPart = prefixSum - target;
                if (hash[counterPart] !== undefined) {
                    ans += hash[counterPart];
                }
                if (hash[prefixSum] === undefined) {
                    hash[prefixSum] = 1
                } else {
                    hash[prefixSum]++;
                }
            }
        }
    }
    return ans;
};