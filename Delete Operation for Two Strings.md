Given two strings word1 and word2, return the minimum number of steps required to make word1 and word2 the same.

In one step, you can delete exactly one character in either string.

 

Example 1:

Input: word1 = "sea", word2 = "eat"
Output: 2
Explanation: You need one step to make "sea" to "ea" and another step to make "eat" to "ea".
Example 2:

Input: word1 = "leetcode", word2 = "etco"
Output: 4
 

Constraints:

1 <= word1.length, word2.length <= 500
word1 and word2 consist of only lowercase English letters.

# Solution
/**
 * @param {string} word1
 * @param {string} word2
 * @return {number}
 */
var minDistance = function(word1, word2) {
    var w1 = word1.length;
    var w2 = word2.length;
    var m = [];
    for (var i = 0; i < w1 + 1; i++) {
        m[i] = [];
        for (var j = 0; j < w2 + 1; j++) m[i][j] = 0;
    }

    for (var i = 1; i <= w1; i++)
        for (var j = 1; j <= w2; j++) {
            if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                m[i][j] = m[i - 1][j - 1] + 1;
            } else {
                m[i][j] = Math.max(m[i - 1][j], m[i][j - 1]);
            }
        }
    return w1 + w2 - 2 * m[w1][w2];
};